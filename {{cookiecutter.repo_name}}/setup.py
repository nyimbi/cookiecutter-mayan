#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys

import {{ cookiecutter.app_name }}

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

version = {{ cookiecutter.app_name }}.__version__

if sys.argv[-1] == 'publish':
    try:
        import wheel
    except ImportError:
        print('Wheel library missing. Please run "pip install wheel"')
        sys.exit()
    os.system('python setup.py sdist upload')
    os.system('python setup.py bdist_wheel upload')
    sys.exit()

if sys.argv[-1] == 'tag':
    print("Tagging the version:")
    os.system("git tag -a %s -m 'version %s'" % (version, version))
    os.system("git push --tags")
    sys.exit()

with open('README.rst') as f:
    readme = f.read()
with open('HISTORY.rst') as f:
    history = f.read().replace('.. :changelog:', '')
with open('LICENSE') as f:
    license = f.read()


setup(
    author='{{ cookiecutter.full_name }}',
    author_email='{{ cookiecutter.email }}',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',   
        'Operating System :: POSIX',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
    ],
    description="""{{ cookiecutter.project_short_description }}""",
    include_package_data=True,
    install_requires=[
    ],
    keywords='{{ cookiecutter.repo_name }}',
    license=license,
    long_description=readme + '\n\n' + history,
    name='{{ cookiecutter.repo_name }}',
    packages=[
        '{{ cookiecutter.app_name }}',
    ],
    url='https://gitlab.com/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.repo_name }}',
    version=version,
    zip_safe=False,
)
