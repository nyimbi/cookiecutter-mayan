from __future__ import absolute_import, unicode_literals

from django.core.urlresolvers import reverse_lazy
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext_lazy as _

# Use Mayan EDMS provided generic class based views
from common.generics import (
    SingleObjectCreateView, SingleObjectDeleteView, SingleObjectDetailView,
    SingleObjectEditView, SingleObjectListView
)
# Since views are processed after models are initialized, they can import
# models directly.
from documents.models import Document

from .models import DocumentExtraData
from .permissions import permission_extra_data_view


class ExtraDataListView(SingleObjectListView):
    model = DocumentExtraData
    view_permission = permission_extra_data_view

    def get_extra_context(self):
        return {
            'hide_object': True,
            'title': _('Extra data list'),
        }


class DocumentExtraDetailView(SingleObjectDetailView):
    model = DocumentExtraData
    view_permission = permission_extra_data_view

    def get_object(self, queryset=None):
        return get_object_or_404(
            Document, pk=self.kwargs['pk']
        ).extra_data

    def get_extra_context(self):
        return {
            # Always return the parent object so that menu can detect them
            # and display the correct navigation links.
            'object': self.get_object().document,
            'title': _(
                'Extra data for document: %s'
            ) % self.get_object().document
        }
