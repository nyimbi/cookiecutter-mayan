from __future__ import unicode_literals

import logging

from django.apps import apps

from mayan.celery import app

from documents.models import Document

logger = logging.getLogger(__name__)


@app.task(ignore_result=True)
def task_extra_data(document_pk):
    # Log beginning of task 
    logger.debug('begin processing document_pk: %s', document_pk)

    Document = apps.get_model(
        app_label='documents',
        model_name='Document'
    )
    
    instance = Document.objects.get(pk=document_pk)
    
    # Perform background task
       
    # Log end of task 
    logger.debug('finished processing document_pk: %s', document_pk)
