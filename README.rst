==================
cookiecutter-mayan
==================

A cookiecutter_ template for creating Mayan EDMS apps quickly.

This template, inspired by `cookiecutter-djangopackage`_, is designed to
allow Mayan EDMS developers create new apps using the established patterns
and conventions.

.. _cookiecutter: https://github.com/audreyr/cookiecutter
.. _cookiecutter-djangopackage: https://github.com/pydanny/cookiecutter-djangopackage

Features
--------

* Sane setup.py for easy PyPI registration/distribution
* Tox configuration
* BSD licensed by default
* Sample apps.py, handlers, links, models, permission, tasks, urls and views modules 

Usage
------

First, install cookiecutter::

    $ pip install cookiecutter

Now run it against this repo::

    $ cookiecutter https://gitlab.com/mayan-edms/cookiecutter-mayan.git

You'll be prompted for some questions, answer them, then it will create a starter Mayan EDMS app.

**Warning**: After this point, change 'Roberto Rosario', 'rosarior', etc to your own information.

It prompts you for questions. Answer them::
    Cloning into 'cookiecutter-mayan'...
    remote: Counting objects: 631, done.
    remote: Compressing objects: 100% (276/276), done.
    remote: Total 631 (delta 329), reused 631 (delta 329)
    Receiving objects: 100% (631/631), 100.89 KiB | 0 bytes/s, done.
    Resolving deltas: 100% (329/329), done.
    Checking connectivity... done.
    full_name [Your full name here]: Roberto Rosario
    email [you@example.com]: roberto.rosario@mayan-edms.com
    gitlab_username [yourname]: rosarior
    project_name [mayan-package]: mayan-extra_data
    repo_name [package]: extra_data
    app_name [package]: extra_data
    project_short_description [Your project description goes here]: Extra document data
    release_date [2016-02-01]: 
    year [2016]: 
    version [0.1.0]: 1.0.0

Enter the project and take a look around::

    $ cd extra_data/
    $ ls

Create a GitLab repo and push it there::

    $ git init
    $ git add .
    $ git commit -m "first awesome commit!"
    $ git remote add origin git@gitlab.com:rosarior/mayan-extra_data.git
    $ git push -u origin master


Running Tests
~~~~~~~~~~~~~~~~~

Code has been written, but does it actually work? Let's find out!

::

    source <YOURVIRTUALENV>/bin/activate
    (myenv) $ pip install requirements/testing.txt
    (myenv) $ tox

Register on PyPI
~~~~~~~~~~~~~~~~~

Once you've got at least a prototype working and tests running, it's time to register the app on PyPI::

    python setup.py register


Releasing on PyPI
~~~~~~~~~~~~~~~~~~~~~~~~

Time to release a new version? Easy! Just run::

    $ python setup.py publish

It will answer with something like::

    You probably want to also tag the version now:
          git tag -a 0.1.0 -m 'version 0.1.0'
          git push --tags

Go ahead and follow those instructions.

Add to Django Packages
~~~~~~~~~~~~~~~~~~~~~~~

Once you have a release, and assuming you have an account there, just go to https://www.djangopackages.com/packages/add/ and add it there.


Follows Best Practices
~~~~~~~~~~~~~~~~~~~~~~~

Follow the Mayan EDMS coding conventions: http://mayan.readthedocs.org/en/latest/topics/development.html

.. image:: http://twoscoops.smugmug.com/Two-Scoops-Press-Media-Kit/i-C8s5jkn/0/O/favicon-152.png
   :name: Two Scoops Logo
   :align: center
   :alt: Two Scoops of Django
   :target: http://twoscoopspress.org/products/two-scoops-of-django-1-8

This project follows best practices as espoused in `Two Scoops of Django: Best Practices for Django 1.8`_.

.. _`Two Scoops of Django: Best Practices for Django 1.8`: http://twoscoopspress.org/products/two-scoops-of-django-1-8

Support This Project
--------------------

This project is maintained by volunteers. Support their efforts by spreading the word about:

.. image:: https://s3.amazonaws.com/tsacademy/images/tsa-logo-250x60-transparent-01.png
   :name: Two Scoops Academy
   :align: center
   :alt: Two Scoops Academy
   :target: http://www.twoscoops.academy/
